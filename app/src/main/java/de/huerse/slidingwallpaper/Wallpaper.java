package de.huerse.slidingwallpaper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;

public class Wallpaper extends WallpaperService {

    private static final String TAG = "Wallpaper";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

    }

    @Override
    public Engine onCreateEngine() {
        return new WallpaperEngine();
    }


    class WallpaperEngine extends Engine {

        private static final String TAG = "WallpaperEngine";

        boolean isOnOffsetsChangedWorking = false;

        private final Paint mPaint = new Paint();
        private Bitmap bitmap;

        int screenWidth;
        int screenHeight;

        int mWidth;
        int mHeight;

        private float _xOffset = 0;

        private GestureDetector mGestureDetector;

        WallpaperEngine() {
            // Create a Paint to draw the lines for our cube
            final Paint paint = mPaint;
            paint.setColor(0xffffffff);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);

            Resources res = getResources();
            bitmap = GlobalSettings.mBackgroundBitmap; //BitmapFactory.decodeResource(res, R.drawable.image);

            WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
            screenHeight = size.y;

            try {
                float factor = (float) screenHeight / (float) bitmap.getHeight();

                float tempwidth = factor * bitmap.getWidth();

                mHeight = screenHeight;
                mWidth = (int) tempwidth;

                float temp = -(bitmap.getWidth() - mWidth);
                bitmap = getResizedBitmap(bitmap, mHeight, mWidth);

                // Bind the gestureDetector to GestureListener
                mGestureDetector = new GestureDetector(getApplicationContext(), new GestureListener());
            }catch(Exception e){}

        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            // By default we don't get touch events, so enable them.
            setTouchEventsEnabled(true);
            setOffsetNotificationsEnabled(true);

            Log.d(TAG, "onCreate");

        }

        @Override
        public void onDestroy() {
            Log.d(TAG, "onDestroy");

            super.onDestroy();
        }

        public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

            int width = bm.getWidth();

            int height = bm.getHeight();

            float scaleWidth = ((float) newWidth) / width;

            float scaleHeight = ((float) newHeight) / height;

            // create a matrix for the manipulation

            Matrix matrix = new Matrix();

            // resize the bit map

            matrix.postScale(scaleWidth, scaleHeight);

            // recreate the new Bitmap

            Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

            return resizedBitmap;

        }
        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            Log.d(TAG, "onSurfaceChanged: width: " + width + ", height: " + height);

        }

        @Override
        public void onOffsetsChanged(float xOffset,float yOffset,
                                     float xStep,float yStep,int xPixels,int yPixels) {
            //_xOffset = xPixels;
            //drawFrame();
        }

        @Override
        public void onTouchEvent(MotionEvent event) {
            //_xOffset -= (float)100;
            mGestureDetector.onTouchEvent(event);
            drawFrame();
            //method onTouchEvent of GestureDetector class Analyzes the given motion event
            //and if applicable triggers the appropriate callbacks on the GestureDetector.OnGestureListener supplied.
            //Returns true if the GestureDetector.OnGestureListener consumed the event, else false.

            //super.onTouchEvent(event);
        }


        public void drawFrame() {
            final SurfaceHolder holder = getSurfaceHolder();
            Canvas c = null;

            try {
                c = holder.lockCanvas();
                c.save();
                if (c != null) {
                    doDraw(c);
                }
            } finally {
                   if (c != null) holder.unlockCanvasAndPost(c);
            }
        }

        public void doDraw(Canvas c) {
            //Rect frameToDraw = new Rect(0, 0, width, height);
            //RectF whereToDraw = new RectF(0, 0, width, height);

            c.translate(_xOffset,0);
            c.drawBitmap(bitmap,0,0, mPaint);
        }

        public class GestureListener extends GestureDetector.SimpleOnGestureListener {
            //float width;
            //float NUMBER_OF_PAGES = 2;

            @Override public boolean onScroll(final MotionEvent e1, final MotionEvent e2, final float distanceX, final float distanceY) {

                //final float newXOffset = _xOffset + distanceX / width / NUMBER_OF_PAGES;
                //if (newXOffset > 1) { _xOffset = 1f; }
                //else if (newXOffset < 0) { _xOffset = 0f; }
                //else { _xOffset = newXOffset; }

                // translate by xOffset;
                //return super.onScroll(e1, e2, distanceX, distanceY);

                float newXOffset = _xOffset;
                newXOffset -= distanceX;

                //final float newXOffset = _xOffset + distanceX / width / NUMBER_OF_PAGES;
                if (newXOffset < - (mWidth-screenWidth-1) ) {
                    //_xOffset = 0f;
                    //_xOffset = - (mWidth-screenWidth);
                    _xOffset = 0;
                } else if (newXOffset > 0) {
                    //_xOffset = 0;
                    _xOffset = - (mWidth-screenWidth);
                    //_xOffset = newXOffset;
                } else {
                    _xOffset = newXOffset;
                }
                // translate by xOffset;
                return super.onScroll(e1, e2, distanceX, distanceY);
            }

            @Override public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX, final float velocityY) {
                //float endValue = velocityX > 0
                        //? (_xOffset - (_xOffset % (1 / NUMBER_OF_PAGES)))
                        //: (_xOffset - (_xOffset % (1 / NUMBER_OF_PAGES)) + (1 / NUMBER_OF_PAGES));

                //if (endValue < 0f) {
                    //endValue = 0f;
                //} else if (endValue > 1f) {
                    //endValue = 1f;
                //}

                //final ValueAnimator compatValueAnimator = ValueAnimator.ofFloat(_xOffset, endValue);
                //compatValueAnimator.setDuration(150);
                //compatValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    //@Override public void onAnimationUpdate(final ValueAnimator animation) {
                        //_xOffset = (float) animation.getAnimatedValue();
                        // translate by xOffset;
                    //};
                    //compatValueAnimator.start();
                //});

                return true;
            }

            @Override public boolean onDoubleTap(final MotionEvent e) {
                // start the Activity
                return true;
            }


        }
    }

}
